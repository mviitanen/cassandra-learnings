\section{Introduction}

\subsection{Problem Space}
Relational databases have dominated data storage for years. In the recent past a group of new technologies, NoSQL databases, have emerged. There are several database engines that are classified as NoSQL, but the only commonality they have is that they are not relational. 

Legacy relational databases have their place in software engineering, and in the enterprise. They are based on a mature technology making them reliable and robust. There is plenty of talent to manage, use, and develop for relational databases.

\subsubsection{ACID}

Relational databases are mainly based on the ACID principles. ACID stands for \textit{Atomicity, Consistency, Isolation, and Durability}.

\paragraph{Atomicity} means that a transaction either works or it doesn't. The classic example is that if you make a money transfer, both the withdrawal from your account and the deposit to the receiving account must occur or not. It is not OK if only one succeeds.
\paragraph{Consistency} means that the data is always correct and up to date. If you deposit money in one bank, immediately after it is successful, you could ask for the balance in another branch and it would be correct. 
\paragraph{Isolation} guarantees that each transaction occurs independently of each other. one transaction doesn't change the conditions for your transaction.
\paragraph{Durability} means that the data remains consistent even afterwards. This includes server crashes, and reboots. \cite{bradberry}

Relational databases are very good at adhering to the ACID principles. They really take care of the data well. 

\subsubsection{Big Data Problem}
Because the amount of the stored data has skyrocketed, relational databases easily become bottlenecks. Often all the data is in one database placing a burden on the hardware.

Sharding (slicing the data and storing it on different physical hardware) is a solution, but in that case someone has to know where to look for the data. Often the software is responsible to know where to go ask for specific piece of the data, either geographic, or based on the model. \cite{bradberry}

Partitioning the data improves performance, but the partitions are still managed by the same database software.

\subsubsection{Replication Problem}
However, these principles, specifically Consistency, also cause the problems that gave rise to NoSQL databases. Mainly because the data has to be consistent, replication becomes a problem. In order to be easily consistent, replication needs to follow the master-slave model. All writes go into one physical node, which is replicated to read-only instances of the database. Of course the slaves are not immediately consistent, but the master is. There are multi-master solutions but they are complicated and costly, and require plenty of configuration.

\subsubsection{Hardware Problem}
Large databases require beefy machines that are expensive. Often the database becomes a single point of failure, risking the functionality of the application. It is very difficult to use commodity hardware for anything significant.

\subsubsection{Scalability Problem}
Because of the Big Data and Hardware problems, scalability becomes an issue. It is not easy to add more machines. Sometimes it is not even possible. The only way to scale up is to build up, buying a bigger machine. This causes the database to have very real limitations and restrictions.

\subsubsection{Performance Problem} I think the management can deal with any of the above problems, but they collectively cause the Performance problem. Because the database is large and it is one one logical machine, it doesn't perform optimally. That will represent itself as a business problem. Customers will be unhappy, which will affect the bottom line. Managers, however, need to understand the underlying issues, though, and how just throwing in more hardware won't be a solution.

\subsection{Solution}
A group of specialized databases, that are not relational, has emerged trying to overcome the problems apparent in big data applications, specifically performance. In order to achieve scalability and performance, something else must be sacrificed. Various NoSQL databases take different approaches, providing better solutions for some applications.

\subsubsection{BASE}

NoSQL databases balance the strict ACID principles with a little bit more relaxed principles, coined BASE.

BASE stands for \textit{Basically Available, Soft State, and Eventual consistency}. It is a fancy acronym for basically one idea, eventual consistency. We can alleviate the before-mentioned problems if we relax the requirement of Consistency. Instead of requiring the data to be absolutely consistent, we can expect it to be eventually consistent.\cite{bradberry}

NoSQL databases keep in mind the ACID principles, but moderating them with BASE principles strike a good balance for many applications. Some applications don't require the data to be absolutely up to date, for example most social media applications.

\subsubsection{Replication}
Cassandra solves the replication problem by forming a net of nodes. You can write to any node and the data is eventually propagated. All nodes are equal. We can define a replication factor for a keyspace (database), that determines on how many of the nodes the same information is stored, to ensure that we always have redundancy, in case one node fails. Cassandra handles remplication across nodes, and even datacenters.

\subsubsection{Hardware}
Cassandra nodes can live on commodity hardware, and each node can be on hardware with very different specifications. 

\subsubsection{Scalability}
Because the hardware is not specialized, we can add or remove nodes cheaply and easily. When a new node is added to a Cassandra cluster, it automatically discovers the other nodes and syncs the data.

\subsubsection{Performance} Cassandra has high capacity writes and linear scalability. We can read from any node. There is no one point of failure or inherent bottleneck. Depending how the data is modeled, however, hot spots may form. Because it is easy to scale up we can alleviate performance easier.

\subsubsection{Not a Silver Bullet}
Cassandra, Neo4J, or other NoSQL databases are not the solution for everything, though. In order to simplify the database management, so that it can be highly scalable, distributed, and performs well, they lacks many features provided by the relational databases. Different kinds of NoSQL databases have different strengths and weaknesses. When deciding the application we need to think of the data requirements and decide which database suits us better. Maybe the solution is that we use two databases. Often a relational database is the right choice.

\subsubsection{Not Relational}
One gleaming difference is the lack of relational data. Often NoSQL databases (maybe excepting graph databases) are not for data analysis. It is very hard to make ad hoc queries. When designing the schema for NoSQL databases we think of the queries, not the data. There are no joins in Cassandra or other NoSQL databases. So when we write the data into the database, we must think of what kinds of SELECT queries we will perform on the data. Instead of joining at query time, we denormalize the data at write time to match the queries. That means that the same piece of data is written several times.\cite{bradberry}

\subsubsection{No Inherent Aggregation}
NoSQL databases do not have aggregation functionality. So, aggregation must happen at the time of writing the data.\cite{bradberry} For example, to know the total  amount a company has billed a customer, we need to add to a counter every time we bill the customer, and subtract when we cancel a bill.

\subsection{Adjacent Technologies}
In this paper I concentrate on my experience with the Cassandra database. It has been chosen as the NoSQL database for FamilySearch. In order to compare it with other databases we need to define some characteristics of the group of databases under the NoSQL umbrella.

\subsubsection{Key-Value Databases} Key-value databases simply store the data in key-value pairs. The value is just a blob of data. There is no structure in it, that the database understands. Parsing of data is done in the application. We use the key to look up information.\cite{sadalage}

\subsubsection{Document Databases} Document databases have a structure to the data, although there is no schema. We can use a document database as an upgraded key-value database. We can look up data based on a key, or a set of fields.\cite{sadalage}

\subsubsection{Column-Family Databases} Column-family databases are also basically key-value databases, but with a nested hierarchy. you can store another aggregate as a value, like a table within a row of a table. You could query the whole row, or cherry-pick fields from the row, even from within the aggregate inside the row.  For example, you could have a row with the key of a customer id, and the columns in the row each have a full collection of key-value pairs containing the information for invoices. You could then ask for all the invoice totals for a customer.\cite{sadalage}

\subsubsection{Graph Databases} Among NoSQL databases, the graph databases are unique. They do not organize the data into rows, or columns, or keys and values. The data in a graph database consist of nodes and edges. It is specialized for storing complex relationships. The nodes could hold key-value data, but usually they are kept very small to improve performance.\cite{sadalage}

Pramod J. Sadalage and Martin Fowler, in \textit{NoSQL Distilled: A Brief Guide to the Emerging World of Polyglot Persistence} present some examples of each type of NoSQL database:

\begin{tabular}{| l | l |}
    \hline
    Data Model & Example Databases \\ \hline\
    Key-Value & \begin{tabular}[t]{@{}l@{}}BerkeleyDB\\LevelDB\\Memcached\\Project Voldemort\\Redis\\Risk\end{tabular}\\ \hline
    Document & \begin{tabular}[t]{@{}l@{}}CouchDB\\MongoDB\\OrientDB\\RavenDB\\Terrastore\end{tabular} \\ \hline
    Column-Family & \begin{tabular}[t]{@{}l@{}}Amazon SimpleDB\\Cassandra\\HBase\\Hypertable\end{tabular} \\ \hline
    Graph & \begin{tabular}[t]{@{}l@{}}FlockDB\\HypergraphDB\\Infinite Graph\\Neo4J\\OrientDB\\Risk\end{tabular} \\ \hline
\end{tabular}

Cassandra started as a key-value database and evolved into a column-family database. \cite{sadalage} \cite{bradberry}